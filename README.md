# OpenAI 大模型开放SDK

---

## 目录

1. 功能测试
2. 程序接入

##  1. 功能测试
### 2.1 代码执行

```java
    private OpenAiSession openAiSession;

    @Before
    public void test_OpenAiSessionFactory() {
        // 1 官网原始 apiHost https://api.openai.com/ - 官网的Key可直接使用
        Configuration configuration = new Configuration();
        configuration.setApiHost("https://xxx/");
        configuration.setApiKey("sk-xxx");
        // 2. 会话工厂
        OpenAiSessionFactory factory = new DefaultOpenAiSessionFactory(configuration);
        // 3. 开启会话
        this.openAiSession = factory.openSession();
    }
```

- 测试的IP和key需要使用自己的，可以使用官方的网址和key，也可以放入国内代理的。
### 2.2 对话
```java
    /**
     * 常用对话模式
     */
    @Test
    public void test_chat_completions_stream_channel() throws JsonProcessingException, InterruptedException {
        // 1. 创建参数
        ChatCompletionRequest chatCompletion = ChatCompletionRequest
                .builder()
                .stream(true)
                .messages(Collections.singletonList(Message.builder().role(Constants.Role.USER).content("1+1").build()))
                .model(ChatCompletionRequest.Model.GPT_3_5_TURBO.getCode())
                .maxTokens(1024)
                .build();

        // 2. 可以在此处直接配置自己的模型, 也可以为空，则使用全局的
        String apiHost = "https://ip/";
        String apiKey = "sk-xxxx";

        // 3. 发起请求
        EventSource eventSource = openAiSession.chatCompletions(apiHost, apiKey, chatCompletion, new EventSourceListener() {
            @Override
            public void onEvent(EventSource eventSource, String id, String type, String data) {
                log.info("测试结果 id:{} type:{} data:{}", id, type, data);
            }

            @Override
            public void onFailure(EventSource eventSource, Throwable t, Response response) {
                log.error("失败 code:{} message:{}", response.code(), response.message());
            }
        });
        // 等待
        new CountDownLatch(1).await();
    }
```

## 3. 程序接入
SpringBoot 配置

```pom
# ChatGPT SDK Config
chatgpt:
  sdk:
    config:
      # 状态；true = 开启、false 关闭
      enable: false
      # 官网地址 
      api-host: 
      # 官网申请 
      api-secret-key: xxxx.xxx
```