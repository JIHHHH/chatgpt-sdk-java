package cn.jihhhh.chatgpt.domain.billing;

import lombok.Data;

/**
 * 对话信息
 *
 * @author JIHHHH
 * @date 2025-01-13
 */
@Data
public class Plan {
    private String title;
    private String id;
}
