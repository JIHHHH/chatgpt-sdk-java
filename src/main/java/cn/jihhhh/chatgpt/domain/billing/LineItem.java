package cn.jihhhh.chatgpt.domain.billing;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 对话信息
 *
 * @author JIHHHH
 * @date 2025-01-13
 */
@Data
public class LineItem {
    /** 模型 */
    private String name;
    /** 金额 */
    private BigDecimal cost;
}
