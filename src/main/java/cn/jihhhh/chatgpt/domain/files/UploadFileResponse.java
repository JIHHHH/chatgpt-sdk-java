package cn.jihhhh.chatgpt.domain.files;

import lombok.Data;

import java.io.Serializable;

/**
 * 对话信息
 *
 * @author JIHHHH
 * @date 2025-01-13
 */
@Data
public class UploadFileResponse extends File implements Serializable {
}
