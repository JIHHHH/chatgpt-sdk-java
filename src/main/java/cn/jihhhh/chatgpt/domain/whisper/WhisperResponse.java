package cn.jihhhh.chatgpt.domain.whisper;

import lombok.Data;

import java.io.Serializable;

/**
 * 对话信息
 *
 * @author JIHHHH
 * @date 2025-01-13
 */
@Data
public class WhisperResponse implements Serializable {
    private String text;
}
