package cn.jihhhh.chatgpt.domain.images;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 对话信息
 *
 * @author JIHHHH
 * @date 2025-01-13
 */
@Data
public class Item implements Serializable {

    private String url;
    @JsonProperty("revised_prompt")
    private String revisedPrompt;
}
