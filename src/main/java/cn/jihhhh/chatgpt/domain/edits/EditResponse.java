package cn.jihhhh.chatgpt.domain.edits;

import cn.jihhhh.chatgpt.domain.other.Choice;
import cn.jihhhh.chatgpt.domain.other.Usage;
import lombok.Data;

import java.io.Serializable;

/**
 * 对话信息
 *
 * @author JIHHHH
 * @date 2025-01-13
 */
@Data
public class EditResponse implements Serializable {

    /** ID */
    private String id;
    /** 对象 */
    private String object;
    /** 模型 */
    private String model;
    /** 对话 */
    private Choice[] choices;
    /** 创建 */
    private long created;
    /** 耗材 */
    private Usage usage;

}
