package cn.jihhhh.chatgpt.session;

/**
 * 创建 OpenAiSession 工厂接口
 *
 * @author JIHHHH
 * @date 2025-01-13
 */
public interface OpenAiSessionFactory {
    OpenAiSession openSession();
}
